#!/usr/bin/python
# coding=utf-8


def task(a0, d, N=10):
    """
    Do a generator that returns lazy arithmetical progress.
    a0 is the initial element.
    d is the delta.
    N is the number of elements should be produced.

    Use yield operator.
    
    Usage:
    list(task(0, 1, 5))
    >>> [0, 1, 2, 3, 4]
    """
    # BEGIN
    for i in xrange(0,N+1):
      yield a0 + d*i
    # END
