#!/usr/bin/python
# coding=utf-8


def task(gen):
    """
    Gen is a generator.
    Iterate it manually using .next() method.
    Collect all received elements into a list and return it.

    Usage:
    task(x for x in [1, 2, 3])
    >>> [1, 2, 3]
    """
    # BEGIN
    res = []
    while True:
      try:
        res.append(gen.next())
      except StopIteration:
        break
    return res
        
    # END

