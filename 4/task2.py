#!/usr/bin/python
# coding=utf-8


def task():
    """
    The file 'task2.txt' keeps information about users.
    In real situation it might be too large to read all it's data.

    Each line has a structure like:
    id;name;balance
    where id is integer and balance is float.

    If the line has leading # symbol, you should skip it.

    Return a lazy sequence of dicts with the keys
    'id', 'name', 'balance' and corresponding values.
    """
    f = open('task2.txt')
    for line in f:  # lazy iteration by lines
        # BEGIN
        if line.startswith('#'):
          continue
        info = line.split(';')
        print info
        mydict = {}
        mydict['id'] = int(info[0])
        mydict['name'] = info[1]
        mydict['balance'] = float(info[2])
        yield mydict
        # END
    f.close()
    
print list(task())
