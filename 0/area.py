#!/usr/bin/python

import sys
import math

x = raw_input('x = ')
figure = raw_input('figure is: ')
try:
	x = float(x)
except:
	print 'error'
	sys.exit(1)
if x <= 0:
	print 'error'
	sys.exit(1)
if figure == 'circle':
	area = math.pi*x*x
	print 'The area is: %.2f' % (area)
elif figure == 'square':
	area = x*x
	print 'The area is: %.2f' % (area)
else:
	print 'error'
	sys.exit(1)
	
