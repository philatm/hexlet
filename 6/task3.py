#!/usr/bin/python
# coding=utf-8


def task(ustring, encodings):
    """
    Функция принимает юникодную строку и список кодировок.
    Для каждой кодировки проверить, можно ли закодировать
    в нее переданный юникод.
    Вернуть соответствующий список элементов True/False.
    """
    # BEGIN
    res = []
    for code in encodings:
      try:
        ustring.encode(code)
        res.append(True)
      except:
        res.append(False)
    return res
    # END
