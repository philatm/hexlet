#!/usr/bin/python
# coding=utf-8


def task(string):
    """
    Функция принимает строку в DOS-кодировке.
    Вернуть строку в кодировке UTF-8.
    """
    # BEGIN
    res = string.decode('cp866').encode('utf-8')
    return res
    # END
