#!/usr/bin/python
# coding=utf-8


def task(objects, sep=u', '):
    """
    Функция принимает последовательность произвольных объектов
    и разделитель.
    Вернуть юникодные представления объектов, соединенные разделителем.
    """
    # BEGIN
    res = sep.join([unicode(x) for x in objects])
    return res
    # END
