#!/usr/bin/python
# coding=utf-8


def task(distances):
    """
    Задание со звездочкой.

    Функция принимает последовательность расстояний.
    Каждое расстояние -- это список/кортеж вида (A, B, l), где:
    A -- строка, имя пункта A;
    B -- строка, имя пункта B;
    l -- положительное число, расстояние между A и B.

    Вернуть словарь, который хранит расстояния
    как между A и B, так и между B и A.

    Пример:
    >>> result = task((
        ('Oslo', 'Chita', 234),
        ('London', 'Astana', 743),
    ))
    >>> result['Oslo']['Chita']    # 234
    >>> result['Chita']['Oslo']    # 234
    >>> result['London']['Astana'] # 743
    >>> result['Astana']['London'] # 743
    """
    # BEGIN
    result = {}
    for tup in distances:
      result[tup[0]] = result.get(tup[0], {})
      result[tup[0]][tup[1]] = tup[2]
      result[tup[1]] = result.get(tup[1], {})
      result[tup[1]][tup[0]] = tup[2]
    return result
    
print task((
        ('Oslo', 'Chita', 234),
        ('London', 'Astana', 743),
    ))
