#!/usr/bin/python

# coding=utf-8


def task(keys, vals):
    """
    Функция принимает два списка/кортежа ключей и значений:
    (k1, k2, ..., kn) и (v1, v2, ..., vm).

    Вернуть словарь вида {k1: v1, k2: v2, ...}.
    Если ключей больше чем значений, лишним ключам назначаются значения None.

    Примеры:
    >>> task(['a', 'b', 'c'], [1, 2, 3])
    {'a': 1, 'b': 2, 'c': 3}

    >>> task(['a', 'b', 'c', 'd', 'e'], [1, 2, 3])
    {'a': 1, 'b': 2, 'c': 3, 'd': None, 'e': None}
    """
    # BEGIN
    result = {}
    result = dict(zip(keys, vals))
    for key in keys:
      if key not in result:
        result[key] = None
    return result
    # END
