# coding=utf-8


def task(seq):
    """
    Функция принимает последовательность (список, кортеж или множество).
    Вернуть словарь вида {V: N}, где V -- элемент последовательности,
    N -- сколько раз элемент встречается в последовательности.

    Пример:
    >>> task([10, 20, 20, 21, 30, 21])
    {10: 1, 20: 2, 21: 2, 30: 1}
    """
    # BEGIN
    seq = list(seq)
    myset = set(seq)
    dictionary = {}
    for key in myset:
      val = seq.count(key)
      dictionary[key] = val
    return dictionary
    # END

