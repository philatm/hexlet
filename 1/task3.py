# coding=utf-8


def task(seq):
    """
    Функция принимает последовательность, каждый элемент которой
    может быть другой последовательностью.

    Вернуть плоский отсортированный список всех элементов, в т.ч. и вложенных.

    Пример:
    >>> task([1, 2, 3, (4, 5), [6, 7], {8, 9}])
    [1, 2, 3, 4, 5, 6, 7, 8, 9]
    """
    # BEGIN
    result = []
    for val in seq:
      if isinstance(val, (list, tuple, set)):
        for i in val:
          result.append(i)
      else:
        result.append(val)
    return sorted(result)
    # END

