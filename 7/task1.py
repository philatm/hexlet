#!/usr/bin/python
# coding=utf-8


def task(payments):
    """
    Функция принимает список кортежей вида (name, payment), где:
    name -- строка, имя клиента,
    payment -- float, сумма платежа.

    Вернуть таблицу вида:
    |           Иванов|     53.00|
    |           Петров|   1245.55|
    |          Сидоров|      5.01|

    На столбец имени отводится 16 символов, на сумму -- 10 символов.
    """
    # BEGIN
    res = ''
    for pay in payments:
      res += '|%16s|%10.2f|\n' % (pay[0], pay[1])
    res = res[:-1]
    return res
    # END
