#!/usr/bin/python
# coding=utf-8


def task(url, id, classes, caption, is_new):
    """
    Функция принимает параметры ссылки (элемента разметки <a>):
    url -- Адрес, куда ведет ссылка.
    id -- Значение атрибута id ссылки.
    classes -- Список или кортеж классов ссылки.
    caption -- Текст ссылки, на который кликает пользователь.
    is_new -- Признак того, откроется ссылка в новой вкладке или нет.
              Если истина, атрибут ссылки target равен _blank,
              иначе он равен _self.
    Пример:
    task('http://google.com', 'google-link', ['link', 'bordered'], 'Visit Google', True)
    >>> <a id="google-link" class="link bordered" href="http://google.com" target="_blank">Visit Google</a>
    """
    # BEGIN
    if is_new:
      target = '_blank'
    else:
      target = '_self'
    someclass = ' '.join(classes)
    link  = {'url':url, 'id':id, 'class':someclass, 'caption':caption, 'target':target}
    #string = '<a id="{id}" class="{class}" href="{url}" target="{target}">{caption}</a>'.format(**link)
    if type(url) is unicode:
       string = u'<a id="{id}" class="{class}" href="{url}" target="{target}">{caption}</a>'.format(**link)
    else:
       string = '<a id="{id}" class="{class}" href="{url}" target="{target}">{caption}</a>'.format(**link)
    return string
