#!/usr/bin/python
# coding=utf-8


def task(func1, func2):
    """
    This function receives two functions func1 and func2
    and returns a third function that returns func2(func1(any arguments there)).
    """
    # BEGIN
    return lambda *args, **kwargs : func2(func1(*args, **kwargs))
    # END
func1 = lambda **kw: sorted(kw.iterkeys())
func2 = lambda keys: ','.join(keys)
print task(func1, func2)(foo=2, test=True, ar=None)
