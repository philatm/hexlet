#!/usr/bin/python


def task(users, funcs):
    """
    The function receives a list of dictionaries like [{field1->value1, field2->value2},...]
    and a dictionary of functions like field-->function-with-one-argument.
    For each user, correct it's values by calling corresponding function with the value of a field.

    Usage:
    >>> task([{'name': 'ivan'}], {'name': lambda name: name.upper()})
    [{'name': 'IVAN'}]
    """
    # BEGIN
    for key in funcs.keys():
      for dict1 in users:
        if key in dict1.keys():
          dict1[key] = funcs[key](dict1[key])
    return users

