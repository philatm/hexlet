#!/usr/bin/python
def task(report):
    """
    Report is a message received from a server.
    Return True of there is no `error` string in the report.
    Otherwise, return False.
    """
    # BEGIN
    if 'error' in report.lower():
      return False
    return True
    # END
    

