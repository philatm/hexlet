#!/usr/bin/python
# coding=utf-8


def task(items, sep=','):
    """
    Items is an itrable object (list/tuple/generator).
    Return a string that has all items in the same order
    divided with the separator.
    Notice that items might have not only strings.
    """
    # BEGIN
    res = sep.join([str(x) for x in items])
    return res
    # END
