#!/usr/bin/python
def task(string, sep=','):
    """
    Function receives a string and a separator.
    The string contains data items devided by the separator.
    Return a list of items without blank symbols at the begin/end of each item.
    Omit blank lines in the result.
    """
    # BEGIN
    res = [x.strip() for x in string.split(sep) if x.strip()]
    return res
    # END
