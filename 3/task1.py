#!/usr/bin/python
def task(users, perm):
    """
    Users is an iterable of users.
    Each user is a dict with the following keys:
    id: int,
    name: str,
    banned: bool,
    perms: iterable of str

    Return a dict like {user_id-->user_name} for thouse users
    who are NOT banned and have specified perm in their's permissions.

    Use dict comprehension.
    """
    # BEGIN
    dictionary = { user['id']:user['name'] for user in users if not user['banned'] and perm in user['perms']}
    return dictionary
    # END
