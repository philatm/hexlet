#!/usr/bin/python
# coding=utf-8


def task(sides):
    """
    Sides is an iterable of triples like (a, b, c).

    Return a list of semi-perimeters for each triple..
    Skip thouse triples that have at least one non-positive element.

    Use list comprehension.
    """
    # BEGIN
    result = [(a+b+c)/2.0 for a,b,c in sides if a > 0 and b > 0 and c > 0]
    return result
    # END
