#!/usr/bin/python
# coding=utf-8


def task(func, args):
    """
    Func is a one-argument function.
    Args is an iterable of arguments.
    Return a generator of function calls for each argument.

    Usage:
    >>> task(lambda x: 2 * x, xrange(1, 5))
    <generator object...>
    """
    # BEGIN
    gen = (func(arg) for arg in args)
    return gen
    # END
