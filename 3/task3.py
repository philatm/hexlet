#!/usr/bin/python
# coding=utf-8


def task(mydict):
    """
    Mydict is a dictionary.
    Omit keys that have None value and return a reversed dict {value-->key}
    Use dict comprehension.
    """
    # BEGIN
    dictionary = { v : k for k, v in mydict.iteritems() if v is not None}
    return dictionary
    # END
